#!/usr/bin/python3
from pygame import mixer
from pygame import time
import file_parser
import light_controller as led_control

TARGET_FRAMERATE = 60

def main():
    print('Welcome to beatsaber-lightshow!')
    light_controller = led_control.get_controller()

    valid_paths = file_parser.get_valid_folders('input/')
    song_selections = []
    for path in valid_paths:
        try:
            curr_data = {}
            # Open data file and read in JSON
            try:
                curr_data = file_parser.parse_info_file(path + '/info.dat')
            except:
                # If it's not lowercase, the fact that it's a valid folder guarantees it's capital
                curr_data = file_parser.parse_info_file(path + '/Info.dat')
            song_selections.append({
                'value': path,
                'text': curr_data['name'] + ' | ' + curr_data['artist'],
                'data': curr_data
            })
        except:
            print('Error creating selection option for', path)

    selected_song_index = -2
    while True:
        print('----------Song Catalogue----------')
        selection_index = 0
        for selection in song_selections:
            print ('(' + str(selection_index) + ')', selection['text'])
            selection_index += 1
        selected_song_index = -2
        while selected_song_index < 0 or selected_song_index >= len(song_selections):
            try:
                selected_song_index = int(input('Please choose a song to play. (-1 to quit) >>'))
                if selected_song_index == -1:
                    print('Goodbye!')
                    exit(0)
            except ValueError:
                print('Please enter an integer number corresponding to the song you want to play.')
        
        song_data = song_selections[selected_song_index]['data']
        song_bpm = song_data['bpm']
        song_path = song_selections[selected_song_index]['value'] + '/' + song_data['song']
        # For now, take the first map in the array
        song_map_filename = song_data['maps'][0]['_beatmapFilename']
        lighting_events = file_parser.parse_beatmap_file(song_selections[selected_song_index]['value'] + '/' + song_map_filename)
        if lighting_events != False:
            play_map(lighting_events, song_path, song_bpm, light_controller)


def play_map(lighting_events, song_path, song_bpm, light_controller):
    song_bps = song_bpm / 60
    """
    Initialize the clock and index to keep track of current lighting event.
    Because events are stored in the array in the order that they occur, we can
    sequentially iterate through the array, checking to see if the time of the
    event has passed yet. If so, fire the event and move on to the next.
    """
    lighting_event_index = 0
    timer = time.Clock()
    mixer.init()
    try:
        mixer.music.load(song_path)
    except:
        print('Error loading', song_path, '| File may not exist.')
        return
    mixer.music.set_volume(1.0)
    timer.tick_busy_loop()
    mixer.music.play()
    print('Song started')
    beats_since_song_start = 0
    while lighting_event_index < len(lighting_events):
        time_since_last_frame = timer.tick_busy_loop(TARGET_FRAMERATE) # Note: tick_busy_loop is resource intensive
        beats_since_song_start += (time_since_last_frame / 1000) * song_bps # Convert from milliseconds to seconds to beats
        curr_event_time = lighting_events[lighting_event_index]['_time']
        if curr_event_time <= beats_since_song_start:
            curr_event_type = lighting_events[lighting_event_index]['_type']
            curr_event_value = lighting_events[lighting_event_index]['_value']
            print('Time since song start', beats_since_song_start)
            set_lighting_state(curr_event_type, curr_event_value, light_controller)
            lighting_event_index += 1
        
        light_controller.update(time_since_last_frame)


def set_lighting_state(event_type, event_value, light_controller):
    event_type_mappings = {
        0: led_control.BACK_LASER,
        1: led_control.RING_NEONS,
        2: led_control.LEFT_LASER,
        3: led_control.RIGHT_LASER,
        4: led_control.CENTER_LIGHT
    }

    event_value_mappings = {
        0: led_control.LIGHT_OFF,
        1: led_control.BLUE_ON,
        2: led_control.BLUE_FLASH,
        3: led_control.BLUE_FADE,
        # 4: Reserved
        5: led_control.RED_ON,
        6: led_control.RED_FLASH,
        7: led_control.RED_FADE,
    }

    if event_type in event_type_mappings.keys() and event_value in event_value_mappings.keys():
        light_controller.set_state(event_type_mappings[event_type], event_value_mappings[event_value])
    else:
        print('Ignored Event: %s --> %s'  % (event_type, event_value))

# If run explicitly as main script, execute main function
if __name__ == '__main__':
    main()