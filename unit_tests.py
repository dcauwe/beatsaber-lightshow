import unittest
import file_parser

# To execute, run: python -m unittest unit_tests.py

class FileParsingTests(unittest.TestCase):
    def test_info_parse(self):
        parse_output = file_parser.parse_info_file('testing/input/info.dat')
        self.assertEqual(parse_output['bpm'], 112)
        self.assertEqual(len(parse_output['maps']), 4)
    
    def test_map_parse(self):
        parse_output = file_parser.parse_beatmap_file('testing/input/Easy.dat')
        self.assertEqual(len(parse_output), 966)
        self.assertEqual(parse_output[0], {"_time":2,"_type":4,"_value":5})
        self.assertEqual(parse_output[20], {"_time":42,"_type":1,"_value":1})

class EventTriggerTests(unittest.TestCase):
    # Ensure that lighting events are in sync with song
    def test_trigger_timing(self):
        pass

    def test_trigger_validity(self):
        pass